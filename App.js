import React, { useEffect, useState, useCallback } from 'react';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import { openSettings } from 'react-native-permissions';
import imagePicker from './src/helpers/imagepickercrop'
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
const callLoaction = (callback) => {
  Geolocation.getCurrentPosition(
    (position) => {
      console.log(position);
      return callback(position);
    },
    (error) => {
      // See error code charts below.
      console.log(error.code, error.message);
    },
    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
  );
}
const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
export default () => {

  const [profileImage, setProfileImage] = useState('')
  const [imageData, setImageData] = useState({})

  const changeProfileImage = useCallback(async () => {
    try {
      const image = await imagePicker()
      setProfileImage(image)
      const uri = image.uri;
      const type = image.type;
      const name = image.fileName;
      const source = {
        uri,
        type,
        name,
      }
      setImageData(source)
    }
    catch (error) { }
  }, [])
  console.log(imageData)
  return (
    <View style={styles.container}>
      {/* <MapView
        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={styles.map}
        region={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}
      >
      </MapView> */}
      <TouchableOpacity onPress={changeProfileImage}>
        <Text>Click</Text>
      </TouchableOpacity>
    </View>
  )
}

