import { requestCameraPermission, requestWriteStoragePermission } from './permission'
import ImagePicker from 'react-native-image-crop-picker';
export default () => new Promise(async (resolve, reject) => {
    if (await requestCameraPermission() && await requestWriteStoragePermission()) {
        ImagePicker.openPicker({
            width: 300,
            height: 100,
            cropping: true
        }, (response) => {
            if (response.uri) {
                resolve(response)
            }
            else {
                reject(response)
            }
        })
        // ImagePicker.openCamera({
        //     width: 300,
        //     height: 400,
        //     cropping: true,
        // }).then(image => {
        //     console.log(image);
        // });
        // ImagePicker.openPicker({
        //     width:300,
        //     height: 100,
        //     cropping: true
        //   }).then(image => {
        //     console.log(image);
        //   });
    }
    else {
        reject({ error: 'no permission' })
    }
})